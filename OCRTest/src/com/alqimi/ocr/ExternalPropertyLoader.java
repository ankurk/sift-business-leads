package com.alqimi.ocr;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ExternalPropertyLoader {

	
	
	static private ExternalPropertyLoader _instance = null;
	InputStream input = null;
	Properties prop = new Properties();

	protected ExternalPropertyLoader() {
		try {
			File f = new File("C:/workspace/OCRTest/lib/OCR.properties");
			//File f = new File("C://softwares/apache-tomcat-8.0.46/conf/sift.properties");
			input = new FileInputStream(f);
			prop.load(input);
		} catch (Exception e) {
			System.out.println("error" + e);
		}
	}

	public Properties getValue() {
		return this.prop;
	}

	static public ExternalPropertyLoader instance() {
		if (_instance == null) {
			_instance = new ExternalPropertyLoader();
		}
		return _instance;
	}
}
