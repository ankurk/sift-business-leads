package com.alqimi.ocr;

import java.io.File;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;

public class MyOCRClass {

	public static void readImagetoText (String sourceImagePath){

		// TODO Auto-generated method stub
	File imgFile = new File(sourceImagePath);
		ITesseract instance = new Tesseract();
		instance.setLanguage("eng");
		instance.setDatapath("C:/workspace/OCRTest/tessdata");
		try
		{
			String result = instance.doOCR(imgFile);
			System.out.println(result);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
	}

}
