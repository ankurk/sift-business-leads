package com.alqimi.ocr;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
public class ConnectMongo {
	
	
	/**
	 * Function pulls the data from mongodb and extracts imageURL from MongoDB .
	 * 
	 */
	static Properties prop = ExternalPropertyLoader.instance().getValue();
	public static void main(String[] args) throws UnknownHostException, ParseException {
		
		JSONParser parser = new JSONParser();
		MongoClient client = new MongoClient(prop.getProperty("MongoDBIp"),Integer.parseInt(prop.getProperty("MongoPort")));
		
		MongoDatabase database = client.getDatabase(prop.getProperty("MongoDBName"));
		MongoCollection<Document> collection = database
				.getCollection("webpage");
		BasicDBObject locationAdvQuery = new BasicDBObject();
        locationAdvQuery.put("contentType", "image/png");
		List<Document> documents = (List<Document>) collection.find(locationAdvQuery).projection(Projections.include(new String[] {
        "baseUrl" })).into(
				new ArrayList<Document>());

               for(Document document : documents){
            	  
            	   String doc=document.toJson();
            	   Object obj = parser.parse((String) doc);
   					JSONObject jsonObject = (JSONObject) obj;
   					String url= (String) jsonObject.get("baseUrl");
   					saveImage(url);
                   System.out.println("BaseURL "+url);
               }
	}
	
	/**
	 * Function extracts image from URL and saves to local disk
	 * 
	 */
	private static void saveImage(String imageUrl) {

		String destinationFile=prop.getProperty("destinationFile");
		try{
	    URL url = new URL(imageUrl);
	    InputStream is = url.openStream();
	    OutputStream os = new FileOutputStream(destinationFile);

	    byte[] b = new byte[2048];
	    int length;

	    while ((length = is.read(b)) != -1) {
	        os.write(b, 0, length);
	    }
	    MyOCRClass ocr = new MyOCRClass();
	    ocr.readImagetoText(destinationFile);
	    is.close();
	    os.close();
	}
		catch (Exception e) {
			e.printStackTrace();
		}

		
	}

}
